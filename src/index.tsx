// 样式
import './assets/css/main.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import actions from './actions';
import store from '@fekit/react-store';
import Root from './root';

ReactDOM.render(
  <Provider store={store(actions)}>
    <Root />
  </Provider>,
  document.getElementById('root')
);
