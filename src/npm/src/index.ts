/**
 * 判断当前窗口是否在活动状态？当活动状态发生变化时触发一个回调事件。
 *
 * change  状态发生变化是触发,激活时或隐藏时
 * active  窗口激活时触发
 * hidden  窗口隐藏时触发
 */

// 兼容处理
const visfix = (): any => {
  let vname = 'hidden';
  ['hidden', 'webkitHidden', 'mozHidden', 'msHidden'].some((item: string) => {
    const doc: any = document;
    const has = doc[item] !== undefined;
    if (has) {
      vname = item;
    }
    return has;
  });
  return { ishidden: vname, event: vname.replace(/hidden/i, 'visibilitychange') };
};

// 核心代码
export default class Vis {
  fun: () => void;
  constructor() {
    this.fun = () => {};
  }
  listen(param: any) {
    const { change = (_state: any) => {}, active = (_state: any) => {}, hidden = (_state: any) => {} } = param;
    const { ishidden, event } = visfix();
    const doc: any = document;
    this.fun = () => {
      const state = !doc[ishidden];
      change(state);
      if (state) {
        active(state);
      } else {
        hidden(false);
      }
    };
    document.addEventListener(event, this.fun);
  }
  remove() {
    const { event } = visfix();
    document.addEventListener(event, this.fun);
  }
}

export const vis = new Vis();
