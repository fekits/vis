# @FEKIT/VIS

监听当前页面是否可见状态，可用于客户端转入后台又转回后触发事件

## 索引

- [演示](#演示)
- [安装](#安装)
- [示例](#示例)
- [版本](#版本)

## 演示

[https://mcui.fekit.cn/#route=plugins/js/vis](https://mcui.fekit.cn/#route=plugins/js/vis/)

## 安装

```
npm i @fekit/vis
```

## 示例

```javascript
import McToast from './mc-toast';

useLayoutEffect(() => {
  // 监听页面可见性变化
  vis.listen({
    active() {
      alert('页面当前可见');
    },
    hidden() {
      alert('页面当前隐藏');
    },
  });
  return () => {
    // 清除监听听
    vis.remove();
  };
}, [home]);
```

## 版本

```
v0.1.0 [Latest version]
1. 使用ts重构
```
