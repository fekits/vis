import React, { useEffect, useLayoutEffect } from 'react';
import { connect } from 'react-redux';
import { LayerView } from '@fekit/react-layer';
import { HOME_GET_DATA } from '../actions/home';
import { vis } from '../npm/src';

const store = ({ home }: any) => ({ home });
const event = (dispatch: any) => {
  return {
    getHomeData() {
      dispatch({
        type: HOME_GET_DATA.name
      });
    }
  };
};
function Home(props: any) {
  const { getHomeData, home = {} } = props;

  useEffect(() => {
    // getHomeData();
    return () => {};
  }, [getHomeData]);

  useLayoutEffect(() => {
    // console.log(home);

    vis.listen({
      active() {
        alert('active');
      },
      hidden() {
        alert('hidden');
      }
    });
    return () => {
      vis.remove();
    };
  }, [home]);

  return (
    <div className="home">
      <h1>HOME</h1>
      {/* <p>this is home page</p> */}
      <LayerView id="home" />
    </div>
  );
}

export default connect(store, event)(Home);
