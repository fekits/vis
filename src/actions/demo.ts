import { call, put } from 'redux-saga/effects';
import axios from 'axios';
// 默认数据
export default {
  conf: {}
};

export function* DEMO_GET_CONF(): any {
  const res: any = yield call(axios.get, '/api/conf');
  console.log('DEMO_GET_CONF', res);
  const { data: { code = 0, data = {} } = {} } = res;
  if (code === 0) {
    yield put({
      type: '$',
      data: {
        key: 'demo.conf',
        val: data
      }
    });
  } else {
    console.log('请求异常');
  }
}
