import { Random } from 'mockjs';

export default {
  'get|/api/news': (e) => {
    const res = {
      code: 0,
      message: '操作成功', // aaa
      data: {
        uuid: Random.id(),
        list: [
          {
            id: Random.id(),
            title: Random.ctitle(),
            content: Random.cparagraph()
          }
        ]
      }
    };
    return res;
  }
};
